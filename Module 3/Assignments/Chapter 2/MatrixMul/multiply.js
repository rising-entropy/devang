function matmul() {
    var r1 = document.getElementById("t1"); //accepting number of rows and columns of matrix
    var c1 = document.getElementById("t2");
    var r2 = document.getElementById("t3");
    var c2 = document.getElementById("t4");
    var R1 = +r1.value;
    var R2 = +r2.value;
    var C1 = +c1.value;
    var C2 = +c2.value;
    //required condition for matrix multiplication is that columns of 1st matrix must equal rows of the 2nd
    if (C1 != R2) {
        alert("Matrix Multiplication not Possible for this Size.");
    }
    else {
        var m1 = [[], []];
        var m2 = [[], []];
        var mul = [[], []];
        //values of 1st matrix
        for (var i = 0; i < R1; i++) {
            for (var j = 0; j < C1; j++) {
                m1[i][j] = parseInt(prompt("Enter Element in Row " + i + " and Column " + j + " for matrix 1"));
            }
        }
        //values of 2nd matrix
        for (var i = 0; i < R2; i++) {
            for (var j = 0; j < C2; j++) {
                m2[i][j] = parseInt(prompt("Enter Element in Row " + i + " and Column " + j + " for matrix 2"));
            }
        }
        document.getElementById("para").innerHTML += "Matrix 1 is:<br>";
        for (let i = 0; i < R1; i++) {
            for (let j = 0; j < C1; j++) {
                document.getElementById("para").innerHTML += m1[i][j] + " ";
            }
            document.getElementById("para").innerHTML += "<br>";
        }
        document.getElementById("para").innerHTML += "<br><br>Matrix 2 is:<br>";
        for (let i = 0; i < R2; i++) {
            for (let j = 0; j < C2; j++) {
                document.getElementById("para").innerHTML += m2[i][j] + " ";
            }
            document.getElementById("para").innerHTML += "<br>";
        }
        mul = multiply(m1, m2);
        document.getElementById("para").innerHTML += "<br><br>Resultant Multiplied Matrix is:<br>"; //print the answer matrix
        for (let i = 0; i < R1; i++) {
            for (let j = 0; j < C2; j++) {
                document.getElementById("para").innerHTML += mul[i][j] + " ";
            }
            document.getElementById("para").innerHTML += "<br>";
        }
    }
}
function multiply(a, b) {
    var aNumRows = a.length, aNumCols = a[0].length, bNumRows = b.length, bNumCols = b[0].length, m = new Array(aNumRows); // initialize array of rows
    for (var r = 0; r < aNumRows; ++r) {
        m[r] = new Array(bNumCols); // initialize the current row
        for (var c = 0; c < bNumCols; ++c) {
            m[r][c] = 0; // initialize the current cell
            for (var i = 0; i < aNumCols; ++i) {
                m[r][c] += a[r][i] * b[i][c];
            }
        }
    }
    return m;
}
//# sourceMappingURL=multiply.js.map