function resulto()
{
    var num: number[] = [];  //array to store the data
    
    var temp:HTMLInputElement = <HTMLInputElement>document.getElementById("t1"); //mag f1
    num[0] = +temp.value;

    var temp:HTMLInputElement = <HTMLInputElement>document.getElementById("t2"); //angle f1
    num[1] = +temp.value;

    var temp:HTMLInputElement = <HTMLInputElement>document.getElementById("t3"); //mag f2
    num[2] = +temp.value;

    var temp:HTMLInputElement = <HTMLInputElement>document.getElementById("t4"); //angle f2
    num[3] = +temp.value;

    var t5:HTMLInputElement = <HTMLInputElement>document.getElementById("t5");
    var t6:HTMLInputElement = <HTMLInputElement>document.getElementById("t6");
    var t7:HTMLInputElement = <HTMLInputElement>document.getElementById("t7");
    var t8:HTMLInputElement = <HTMLInputElement>document.getElementById("t8");

    num[1] = num[1]*Math.PI/180; //degree to radian
    num[3] = num[3]*Math.PI/180;

    num[4] = num[0]*Math.cos(num[1]); //X components
    num[5] = num[2]*Math.cos(num[3]);

    num[6] = num[0]*Math.sin(num[1]); //Y components
    num[7] = num[2]*Math.sin(num[3]);

    num[8] = num[4] + num[5]; //Xcomponent resultant
    num[9] = num[6] + num[7]; //Ycomponent resultant

    num[10] = Math.sqrt(num[8]*num[8] + num[9]*num[9]); //resultant magnitude

    

    num[11] = Math.atan2(num[8], num[9]); //resultant angle in radian
    num[12] = (num[11]*180/Math.PI)%360; //radian to degree

    t5.value = num[8].toString();
    t6.value = num[9].toString();
    t7.value = num[10].toString();
    t8.value = num[12].toString();
}

//The non-atan2()appraoch:
/*if(num[8]<0 && num[9]<0)//3rd quadrant
    {
        num[11] = Math.atan(num[9]/num[8]); //resultant angle in radian
        num[12] = 180 + (num[11]*180/Math.PI)%360; //radian to degree
    }
    else if(num[8]>=0 && num[9]<0)//2nd quadrant
    {
        num[11] = Math.atan(num[9]/num[8]); //resultant angle in radian
        num[12] = 90 + (num[11]*180/Math.PI)%360; //radian to degree
    }
    else if(num[8]<0 && num[9]>=0) //4th quadrant
    {
        num[11] = Math.atan(num[9]/num[8]); //resultant angle in radian
        num[12] = 270 + (num[11]*180/Math.PI)%360; //radian to degree
    }
    else //1st quadrant
    {
        num[11] = Math.atan(num[9]/num[8]); //resultant angle in radian
        num[12] = (num[11]*180/Math.PI)%360; //radian to degree
    }*/