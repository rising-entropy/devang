let list1:HTMLSelectElement = <HTMLSelectElement>document.getElementById("fruit"); //creating the list
let fruit:{value:number, name:string, type:string, price:number}[] = [];

fruit.push({ value:0, name: "Mango", type:"Regular", price: 600}); //adding available items
fruit.push({ value:1, name: "Mango", type:"Organic", price: 800});
fruit.push({ value:2, name: "Watermelon", type:"Regular", price: 100});
fruit.push({ value:3, name: "Watermelon", type:"Organic", price: 200});
fruit.push({ value:4, name: "Orange", type:"Regular", price: 150});
fruit.push({ value:5, name: "Orange", type:"Organic", price: 400});
fruit.push({ value:6, name: "Jack Fruit", type:"Regular", price: 300});
fruit.push({ value:7, name: "Jack Fruit", type:"Organic", price: 500});
fruit.push({ value:8, name: "Chickoo", type:"Regular", price: 100});
fruit.push({ value:9, name: "Chickoo", type:"Organic", price: 150});
fruit.push({ value:10, name: "Cherry", type:"Regular", price: 50});
fruit.push({ value:11, name: "Cherry", type:"Organic", price: 100});

for(let i= 0; i<fruit.length; i++)        //adding as options
{
    let option:HTMLOptionElement = <HTMLOptionElement>document.createElement("option");
    option.text = fruit[i].name + "--" + fruit[i].type;
    option.value = fruit[i].value.toString();
    list1.add(option);
}

function price()                              //get type and quantity from user and display price
{
    //getting the value for quantity
    let qty:HTMLInputElement = <HTMLInputElement>document.getElementById("qty");
    var qty_ = +qty.value;
    
    //check for invalid number
    if(isNaN(qty_))
    {
        alert("Please enter a Valid Input");
    }
    else
    {
        let ans:number = list1.selectedIndex;          //DOM Manipulation
        var namer=fruit[ans].name + "--" + fruit[ans].type;
        var pricer = fruit[ans].price;
    let z:HTMLInputElement = <HTMLInputElement>document.getElementById("cart");     //display choice and price
    z.innerHTML = z.innerHTML+namer+"="+pricer*qty_+"<br>";
    }
}


