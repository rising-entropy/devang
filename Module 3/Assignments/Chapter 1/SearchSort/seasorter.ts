function sorter()
{
    document.getElementById("para").innerHTML = "";
    var num: number[] = []; 

    var x:number =  parseInt(prompt("Enter the number of Elements: "));

    for(let i=1; i<=x; i++)  //taking in array values
    {
        num[i] = parseInt(prompt("Enter Element No. " + i));
    }
    
    document.getElementById("para").innerHTML += "The Entered Array is:<br>";

    for(let i=1; i<=x; i++)
    {
        document.getElementById("para").innerHTML += num[i]+"<br>";
    }

    for(let i=1; i<=x; i++) //bubblesort algorithm
    {
        for(let j=0, stop=x-i; j<stop; j++)
        {
            if(num[j]>num[j+1])
            {
                let temp = num[j];
                num[j] = num[j+1];
                num[j+1] = temp;
            }
        }
    }

    document.getElementById("para").innerHTML += "The Sorted Array is:<br>";

    for(let i=1; i<=x; i++)
    {
        document.getElementById("para").innerHTML += num[i]+"<br>";
    }
}

function searcher()
{
    document.getElementById("para").innerHTML = "";

    var num: number[] = []; 

    var x:number =  parseInt(prompt("Enter the number of Elements: "));

    for(let i=1; i<=x; i++) //taking in array values
    {
        num[i] = parseInt(prompt("Enter Element No. " + i));
    }
    
    document.getElementById("para").innerHTML += "The Entered Array is:<br>";

    for(let i=1; i<=x; i++)
    {
        document.getElementById("para").innerHTML += num[i]+"<br>";
    }

    var k:number =  parseInt(prompt("Enter Element to Search: "));
    var w:number = 0;

    for(let i=1; i<=x; i++) //performing linear search
    {
        if(num[i] == k)
        {
            w = i;
        }
    }

    if(w == 0)
    document.getElementById("para").innerHTML += "<br>Element Does Not Exist.";
    else
    document.getElementById("para").innerHTML += "<br>Element is at Pos. "+w;
}