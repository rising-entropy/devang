# ZENER DIODE APPLICATIONS

## Zener Diode as a Voltage Regulator

### Aim:
To use Zener diode as voltage regulator. Measurement of
percentage regulation by varying load resistor.

![setup](setup.jpg)

### Theory:
Zener diode is a P-N junction diode specially designed to operate in the reverse biased mode. It
is acting as normal diode while forward biasing. It has a particular voltage known as break down
voltage, at which the diode break downs while reverse biased. In the case of normal diodes the
diode damages at the break down voltage. But Zener diode is specially designed to operate in the
reverse breakdown region.

The basic principle of Zener diode is the Zener breakdown. When a diode is heavily doped, it’s
depletion region will be narrow. When a high reverse voltage is applied across the junction, there
will be very strong electric field at the junction. And the electron hole pair generation takes
place. Thus heavy current flows. This is known as Zener break down.

So a Zener diode, in a forward biased condition acts as a normal diode. In reverse biased mode,
after the break down of junction current through diode increases sharply. But the voltage across
it remains constant. This principle is used in voltage regulator using Zener diodes. 

![circuit](zener.png)

### Procedure:
#### Input Characteristics:
1. Varying the input voltage keeping load constant: Connect the circuit as showin. Keep supply control at minimum.
1. Keep the load RL at 750ohms for Q-pont. Increase the input voltage VS in step of 1Volt and note V1 and V2. Where V1 is the input and V2 is the output voltage across zener.
1. Plot the curves between input –output at load constant. Find out the δV1 and δV2 from the plot and calculate the line regulation.

#### Output Characteristics:
1. Varying the load keeping input voltage constant: Connect the circuit as shown. Keep supply control at minimum.
1. Keep load RL at 3000ohms. Increase the input voltage V1 to 12 Vdc.
1. Decrease the load and note the voltage V2 with load value.
1. Plot the curves between load and output voltage at input constant. Find out the δV2 and VZ at Q point at set load value from the input plot and calculate load regulation.

**Note:** 
V2 is zener voltage at minimum and maximum load current in stable region. The Q point is fixed in input characteristic plot. The input voltage held at 12V constant. The δV2 is very
small note it carefully. 
