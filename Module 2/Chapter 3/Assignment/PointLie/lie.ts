function inout()
{
    var ax:HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
    var ay:HTMLInputElement = <HTMLInputElement>document.getElementById("t2");
    var bx:HTMLInputElement = <HTMLInputElement>document.getElementById("t3");
    var by:HTMLInputElement = <HTMLInputElement>document.getElementById("t4");
    var cx:HTMLInputElement = <HTMLInputElement>document.getElementById("t5");
    var cy:HTMLInputElement = <HTMLInputElement>document.getElementById("t6");
    var px:HTMLInputElement = <HTMLInputElement>document.getElementById("t7");
    var py:HTMLInputElement = <HTMLInputElement>document.getElementById("t8");
    var ans:HTMLInputElement = <HTMLInputElement>document.getElementById("ans");

    var x1 = +ax.value;
    var x2 = +bx.value;
    var x3 = +cx.value;
    var x = +px.value;
    var y1 = +ay.value;
    var y2 = +by.value;
    var y3 = +cy.value;
    var y = +py.value;

    var abc = Math.abs((x1*(y2-y3) + x2*(y3-y1) + x3*(y1-y2))/2);
    var pab = Math.abs((x*(y1-y2) + x1*(y2-y) + x2*(y-y1))/2);
    var pbc = Math.abs((x*(y2-y3) + x2*(y3-y) + x3*(y-y2))/2);
    var pac = Math.abs((x*(y1-y3) + x1*(y3-y) + x3*(y-y1))/2);

    var sum = pab + pbc + pac;

    if(Math.abs(abc - sum) == 0)
    {
        ans.value = "Point Lies Inside The Triangle";
    }
    else
    {
        ans.value = "Point Lies Outside The Triangle";
    }
}