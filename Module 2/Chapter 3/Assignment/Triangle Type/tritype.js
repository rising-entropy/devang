function odde() {
    var a = document.getElementById("t1");
    var n = parseFloat(a.value);
    if (isNaN(n)) {
        alert("Enter the Number.");
    }
    else {
        var d = +a.value;
        if (d % 2 == 0)
            alert("The Number is Even.");
        else
            alert("The Number is Odd.");
    }
}
function typee() {
    var a = document.getElementById("t1");
    var b = document.getElementById("t2");
    var c = document.getElementById("t3");
    var ans1 = document.getElementById("ans1");
    var ans2 = document.getElementById("ans2");
    var p = +a.value;
    var q = +b.value;
    var r = +c.value;
    var e1 = Math.sqrt(p * p + q * q);
    var e2 = Math.sqrt(q * q + r * r);
    var e3 = Math.sqrt(p * p + r * r);
    if (p == q && q == r) {
        ans1.value = "Equilateral Triangle";
    }
    else if (p == q || p == r || q == r) {
        ans1.value = "Isoceles Triangle";
    }
    else {
        ans1.value = "Scalene Triangle";
    }
    if (e1 == r || e2 == p || e3 == q) {
        ans2.value = "Right Angled Triangle";
    }
    else {
        ans2.value = "Not Right Angled Triangle";
    }
}
//# sourceMappingURL=tritype.js.map