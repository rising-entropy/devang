function triangle()
{
    var v1: HTMLInputElement = <HTMLInputElement>document.getElementById("x1");
    var v2: HTMLInputElement = <HTMLInputElement>document.getElementById("x2");
    var v3: HTMLInputElement = <HTMLInputElement>document.getElementById("x3");
    var w1: HTMLInputElement = <HTMLInputElement>document.getElementById("y1");
    var w2: HTMLInputElement = <HTMLInputElement>document.getElementById("y2");
    var w3: HTMLInputElement = <HTMLInputElement>document.getElementById("y3");

    var x1:number = +v1.value;
    var x2:number = +v2.value;
    var x3:number = +v3.value;
    var y1:number = +w1.value;
    var y2:number = +w2.value;
    var y3:number = +w3.value;

    var ans: HTMLInputElement = <HTMLInputElement>document.getElementById("ans");

    var a: number = Math.sqrt((x1 - x2)*(x1 - +x2) + (y1 - y2)*(y1 - y2));
    var b: number = Math.sqrt((x1 - x3)*(x1 - +x3) + (y1 - y3)*(y1 - y3));
    var c: number = Math.sqrt((x2 - x3)*(x2 - +x3) + (y2 - y3)*(y2 - y3));

    var s: number = (a + b + c)/2;

    var area:number = Math.sqrt(s*(s-a)*(s-b)*(s-c));

   ans.value = area.toString();
}