var ans = document.getElementById("ans");
function add() {
    var first = document.getElementById("first");
    var second = document.getElementById("second");
    var c = parseFloat(first.value) + parseFloat(second.value);
    ans.value = c.toString();
}
function sub() {
    var first = document.getElementById("first");
    var second = document.getElementById("second");
    var c = parseFloat(first.value) - parseFloat(second.value);
    ans.value = c.toString();
}
function mul() {
    var first = document.getElementById("first");
    var second = document.getElementById("second");
    var c = parseFloat(first.value) * parseFloat(second.value);
    ans.value = c.toString();
}
function div() {
    var first = document.getElementById("first");
    var second = document.getElementById("second");
    var c = parseFloat(first.value) / parseFloat(second.value);
    ans.value = c.toString();
}
function sin() {
    var first = document.getElementById("first");
    var q = +first.value;
    var c = Math.sin(q);
    ans.value = c.toString();
}
function cosine() {
    var first = document.getElementById("first");
    var q = +first.value;
    var c = Math.cos(q);
    ans.value = c.toString();
}
function tangent() {
    var first = document.getElementById("first");
    var q = +first.value;
    var c = Math.tan(q);
    ans.value = c.toString();
}
function power() {
    var first = document.getElementById("first");
    var second = document.getElementById("second");
    var p = +first.value;
    var q = +second.value;
    var c = Math.pow(p, q);
    ans.value = c.toString();
}
//# sourceMappingURL=calc.js.map