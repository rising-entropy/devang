function cosine()
{
    var a: HTMLInputElement = <HTMLInputElement> document.getElementById("x");
    var b: HTMLInputElement = <HTMLInputElement> document.getElementById("ans");

    var q: number = +a.value;
    var p: number = q + Math.cos(q);

    b.value = p.toString();
}